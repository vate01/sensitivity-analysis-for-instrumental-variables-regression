
\documentclass[11pt, leqno, letterpaper]{article}

\listfiles

% \usepackage[T1]{fontenc}
% \usepackage[utf8]{inputenc}
\usepackage{fontspec}
\usepackage{libertine}
\usepackage[english]{babel}
\usepackage{authblk}

\usepackage{amsmath, amsthm, amssymb, amsfonts}
% \usepackage[round]{natbib}

\usepackage[bibencoding=utf8, natbib=true, backend=biber, hyperref=true, sorting=nty, sortcites=true, style=authoryear, % citestyle=apa,
citetracker=true, maxcitenames=1, maxbibnames=99]{biblatex} 
\usepackage{csquotes}

\renewbibmacro{in:}{}
\usepackage{xpatch}

% No dot before number of articles
\xpatchbibmacro{volume+number+eid}{%
  \setunit*{\adddot}%
}{%
}{}{}

% Number of articles in parentheses
\DeclareFieldFormat[article]{number}{\mkbibparens{#1}}

% \DeclareFieldFormat{citefield}{\mkbibbold{#1}}

% \preto\fullcitet{\AtNextcitet{\defcounter{maxnames}{99}}}
\AtEveryCitekey{\ifciteseen{}{\defcounter{maxnames}{99}}}

\addbibresource{main.bib}
\renewcommand*{\bibfont}{\footnotesize}

\usepackage{graphicx}
\usepackage{float} % fuerza con H el lugar de las imágenes y tablas
\usepackage{tikz} % líneas
\usepackage{pgfplots} % líneas
\pgfplotsset{compat=1.12} 
\usepackage{caption} % subtítulos centradas
\usepackage{subcaption}
% \usepackage{enumerate}
\renewcommand{\theenumi}{\roman{enumi}}

\usepackage{multirow}
\usepackage{booktabs} % líneas en las tablas
% \usepackage{verbatim} % comentarios largos

\usepackage{makecell}
% \usepackage[modulo,pagewise]{lineno}
% \linenumbers

\usepackage[left=2cm, right=2cm, bottom=2.5cm, top=2cm]{geometry}
\usepackage{setspace} % http://www.tex.ac.uk/FAQ-linespace.html
% \onehalfspacing
\doublespacing

\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, % Colours links instead of ugly boxes
  urlcolor     = black, % Colour for external hyperlinks
  linkcolor    = black, % Colour of internal links
  citecolor    = black  % Colour of citations
}

\title{Sensitivity Analysis for Instrumental Variables Regression:\\
A Nonparametric Bayesian Approach}

\author[1]{Freddy Omar López Quintero}

\affil[1]{{\small Doctorado en matemática, PUCV-UTFSM-UV, Valparaíso, Chile.}}

\date{}

\begin{document}

\maketitle

% \begin{center}
% Supervised by:\\
% Emilio Porcu \& Federico Crudu
% \end{center}

\begin{abstract}
% \input{abstract}
Instrumental variable methods play an important role in regression analysis when one or more predictors are endogenous. A variable qualifies as an instrument when certain assumptions are met. In particular, it must be uncorrelated with the error term of the structural equation, correlated with the endogenous variable and not influence directly over the response variable. A sensitivity analysis is proposed for the case of not entirely exogenous instrumental variables. In this article it is proposed a Nonparametric Bayesian Approach to Instrumental Variable Regression when exclusion restriction assumption is suspected to not be hold.

\vspace{2mm}
\emph{Keywords}: Regression Analysis, Instrumental variables, Sensitivity analysis, Bayesian analysis.
\end{abstract}

% \input{body}


\section{Introduction and Sensitivity analysis}\label{sa}


The way to introduce the assumptions on which the estimation of

\begin{equation}
\begin{aligned}
        y&=Z\beta_1+Y\beta_2+u\\
        Y&= Z\Pi_1+W\Pi_2+V,
\end{aligned} \label{iv}
\end{equation}

\noindent could be performed, usually vary from potential outcome/observational studies and econometric sources. In the former, emphasis is placed on that instruments meet that i. \emph{must be related} with the endogenous variables in $Y$ and
ii. they should not influence \emph{directly} on the response variable, $y$. These two assumptions can be represented as in Figure \ref{figure_assumptions:a}. Second item, ii., is usually called `exclusion restriction' by potential outcome experts \citep{hernan2010causal}. When we turn our sight to econometric literature, the fundamental assumption to estimate parameters in system (\ref{iv}) is $\text{cov}(W,u)=0$, that is, instruments are not correlated with the random perturbations. This requirement is necessary to obtain consistent estimator. \citet{angrist2008mostly} pointed out that this assumption is like saying that instruments are uncorrelated with both, $y$ and $u$. \citet{angrist1996identification} treated to conciliate both perspectives establishing that the no presence of $W$ in the first equation of system (\ref{iv}) together with $\text{cov}(W,u)=0$, collect the idea of the exclusion restriction assumption. In any case, there are situations where is reasonable to be suspicious about validity of instruments \citep{chetty2011kindergarten}. 

\begin{figure}[H]
% \begin{figure}[!t]
\centering 
	\begin{minipage}[b]{0.4\linewidth}
		\centering \captionsetup{justification=centering}

\begin{tikzpicture}[scale=2.5,every path/.style={>=latex},every node/.style={draw,circle}]
  \node [draw=none] (a) at (-1,0)  { $W$ };
  \node [draw=none] (b) at (0,1)  { Y };
  \node [draw=none] (c) at (1,0) { y };

  \path[->] (a) edge node [above =.1 cm, xshift=-.4cm, yshift=-.4cm, draw=none] {$\Pi_2$} (b);
  \path[->, white] (a) node [below =.1 cm, draw=none] {$\tau$} (c);
  \path[->] (b) edge node [above =.1 cm, xshift=.4cm, yshift=-.3cm, draw=none] {$\beta_2$} (c);
  \end{tikzpicture}

		\subcaption{} \label{figure_assumptions:a}
	\end{minipage}%
	\begin{minipage}[b]{0.4\linewidth}
		\centering \captionsetup{justification=centering}

\begin{tikzpicture}[scale=2.5,every path/.style={>=latex},every node/.style={draw,circle}]
  \node [draw=none] (a) at (-1,0)  { $W$ };
  \node [draw=none] (b) at (0,1)  { Y };
  \node [draw=none] (c) at (1,0) { y };

  \path[->] (a) edge node [above =.1 cm, xshift=-.4cm, yshift=-.4cm, draw=none] {${\Pi}_2$} (b);
  \path[->] (a) edge node [below =.1 cm, draw=none] {$\tau$} (c);
  \path[->] (b) edge node [above =.1 cm, xshift=.4cm, yshift=-.3cm, draw=none] {${\beta}_2$} (c);
  \end{tikzpicture}

		\subcaption{} \label{figure_assumptions:b}
	\end{minipage}
\caption{Exclusion restriction representation (omitting included exogenous variables, $Z$, and random perturbations, $u,V$) for (\subref{figure_assumptions:a}) valid and (\subref{figure_assumptions:b}) invalid $W$ instruments.}	\label{figure_assumptions}
\end{figure}


% In this sense, many authors have addressed the DEBILIDAD and have called it with different manners.

% Kinds of violation of exclusion restriction assumption

% in the econometric field, many researchers have lost the causal interpretation suppressed by their interest in parameter estimation VERIFICAR

As a consequence, to our understanding, there have been at least two main different branches, even related, approaches to explore the exclusion restriction violation.  One of them, focused on controlling and measuring the covariance structure between instruments and error; and the other has been based on controlling and measuring the direct effect of the IVs over the response by an extra parameter in the first equation of the system (\ref{iv}).

More in detail, the flawness in the covariance structure has been recently studied by \citet{hahn2005estimation,berkowitz2012validity} and \citet{nevo2012identification}, among others. For instance, \citet{ashley2009assessing,ashley2015sensitivity,ashley2015justifiable} worked around the weakness in $\text{cov}(W,u)$, what they called the exogeneity flaw covariance vector. Under this setting, they proved that the IV estimator 
for $\beta=(\beta_1,\beta_2)$ depend indeed on a $\text{cov}(W,u)=\Sigma_{W,u}\neq 0$, such as

\begin{equation}
\text{plim}(\hat{\beta})=\text{plim} \left[ \left( \frac{1}{n} W^\top Y\right)^{-1} \left( \frac{1}{n} W^\top y\right) \right]  =\beta+\Sigma_{W,Y}^{-1}\Sigma_{W,u}
\end{equation}

\noindent Unfortunately, there is no knowledge about what is the true value for $\text{cov}(W,u)$ and, consequently, they proposed to do a search over different possible values of violation of the exogeneity. When dimension of the $W$ is 1, search is reduced to test through for a grid of different posited values whereas for greater dimensions, some kind of stochastic search must be performed. When this assumption is not accomplished, \citet{kiviet2016discriminating} refer to it as invalid instruments due to a questionable exclusion restriction schema.

On the other side, when the assumption of non-direct influence of the IVs over the response variable is relaxed, research have been based on the following modification of system (\ref{iv})

\begin{equation}
\begin{aligned}
        y&=Z\beta_1+W\tau+Y\beta_2+u\\
        Y&= Z\Pi_1+W\Pi_2+V,
\end{aligned}\label{iv_mod}
\end{equation}

\noindent where the attention, instead of be on the covariance structure, is over the new $\tau$ parameter. This violation is represented in Figure \ref{figure_assumptions:b}, where is theorized that $\tau$ measures the amount of direct influence of instruments on $y$. 
It must be noted that to reach consistency in the estimation even when $\tau=0$, it is necessary that $\text{cov}(W,u)=0$.

Some of the exponents of this particular approach are \citet{small2007sensitivity,jiang2013sensitivity,conley2012plausibly} and some extensions to panel data such as in \citet{block2012education}. In particular, \citet{small2007sensitivity} procedure can be (extremely) summarized as follow i. look for a candidate value, $\tau_0$, for $\tau$. \citet{small2007sensitivity} describes a procedure to get an idea of the initial values $\tau$ could take, ii. restrict the attention to those pairs of values, $\beta_2$ and $\tau_0$, where Anderson and Rubin and Sargan tests are accepted. 

The purpose of the first test is ensure that $\beta_2$ values are reasonable to our hypothesis while the second test is to make certain that not extra overidentifying restrictions involved in the estimation are present. Naturally, the suggested procedure is not limited to use Anderson and Rubin or Sargan tests \citep{jiang2013sensitivity,guo2016}. % Actually, given that the validity of the overidentifying restrictions is not necessary for the parameter identifiability \citep{parente2012cautionary}, the second step turned just into a good extra step \citep[it has been also called `pretest' step, see][]{kang2016robust}.

% Actually, second step turn into an extra step as it is always possible report values of $\beta$ (given the $\tau_0$ chosen) and 

% In a serie of subsequent works, those ideas have evolved to consider (probably) many (invalid) instruments such as in \citet{kang2016robust,kang2016instrumental}. An important issue to take into account is that some approaches when information about the specific validity of each instruments is available, it is incorporated into the estimation procedure \citep{gautier2011high,belloni2012sparse,kolesar2015identification}. For instance, \citet{liao2013adaptive} assumes there exist a clear knowledge about what instruments could be valid and which invalid REVISAR. In fact, and perhaps the most delicate point that \citet{small2007sensitivity} and \citet{ashley2009assessing,ashley2015sensitivity,ashley2015justifiable} procedures have in common, is precisely the needed of some extra information or condition to allow the identification of endogenous parameter, $\beta_2$.

% in order to have an estimation of endogenous parameter, $\beta_2$, it is needed to have a previous idea of the violation parameter, either $\tau$ or $\text{cov}(W,u)$, to establish 

% IN THIS WORK WE WILL TAKE THIS LAST AS THE VIOLATION EXCLUSION RESTRICTION ASSUMPTION


Referring to suspicious assumption validity, \citet{conley2012plausibly} provided a Bayesian method to work with plausibly exogenous of the IVs. Indeed, \citet{conley2012plausibly} made several proposals including, mostly, the particular approach followed by \citet{small2007sensitivity}. Throughout the paper, the last concept of violation of exclusion restriction will be adopted. In next section we describe some Bayesian approaches that have been generally used to deal with IVs problem to finally lay the foundations of our modest contribution. % COMPLETAR

% AÑADIR AQUÍ PROBLEMAS CON LA VEROSIMILITUD

% Taking into account the complications related with likelihood based method, we appeal to a Bayesian Method of Moment like estimator and marginalizing respect to PARAMETROS DE VIOLACION.

% We treat the situation where we can manage the possibility to introduce our knowledge about instrument validity 

% In this research we are keeping the setting simple

% \input{old}

% In the next section, we lay the foundations of our contribution. % Firstly, BLABLABLA. Secondly, BLABLABLA.

% \section{Proposal}



% Numerical Integration

\section{Bayesian Generalized Method of Moments}\label{ba}

% $\Sigma_{u,V}$ then $ \begin{pmatrix}
%   y \\
%   Y
%  \end{pmatrix} \sim \text{Normal}\left( 
%  \begin{pmatrix}
%   \widetilde{Y}\beta \\
%   \widetilde{Z}\Pi
%  \end{pmatrix},
%  \Sigma_{u,V}
%  \right) $

% IVs treatment , 


% The basic  is as follows and is drawn from Rossi

% We start describing some pure Bayesian IV approaches. To make the notation shorten, let be $\widetilde{Y}=(Z,Y)$, $\beta=(\beta_1,\beta_2)$, $\widetilde{Z}=(Z,W)$ and $\Pi=(\Pi_1,\Pi_2)$; therefore

% \begin{equation}
% \begin{aligned}
%         y&=\widetilde{Y}\beta+u\\
%         \widetilde{Y}&=\widetilde{Z}\Pi+V,
% \end{aligned}
% \end{equation}

% \noindent and consequently $(y,\widetilde{Y})^\top \sim \text{Normal}((\widetilde{Y}\beta, \widetilde{Z}\Pi)^\top,\Sigma_{u,V})$. An important amount of effort by Bayesian researchers has been put into the study and development of prior distributions. 

% In the informative case, \citet{greenberg2008} stated a direct Gibbs sampler procedure by imposing natural conditional distributions such as normal for $(\beta, \Pi)$ and an inverse Wishart for $\Sigma_{u,V}$. It is successfully completed by taking advantage of join likelihood decomposition. Conditional posterior distributions follow likewise normal and inverse Wishard, respectively.
% To complete the model specification, it is needed to put prior distributions over parameters $\beta, \Pi$ and $\Sigma_{u,V}$. 

% According to 

% \citet{rossi2005bayesian} also described a similar procedure. Moreover, they discussed that it is possible to handle prior distributions over the reduced form of the model 

% \begin{equation*}
% \begin{aligned}
%         y&=\widetilde{Z}\Pi_{\widetilde{Y}}+\nu\\
%         \widetilde{Y}&=\widetilde{Z}\Pi+V,
% \end{aligned}
% \end{equation*}

% \noindent with $y=[\widetilde{Z}\Pi+V]\beta+u=\widetilde{Z}\Pi\beta+V\beta+u=\widetilde{Z}\Pi_{\widetilde{Y}}+\nu$. $\beta$ and $(\Pi,\Pi_{\widetilde{Y}})$ are clearly related by expression $\Pi\beta=\Pi_{\widetilde{Y}}$.

% defining appropriate constrains over new parameters  



% \noindent where 

% What make the difference

% COMPLETAR CON RESUMEN DEL BIV

% Under flat priors, \citet{zellner2014bayesian} studied the existence of proper and marginal posterior distributions whilst \citet{lopes2014bayesian} summarized and discussed the Jeffrey's prior and proposed a new Cholesky-based prior for covariance matrix. \citet{cogley2012bayesian} extended the Gibbs sampler to multiple endogenous variables.

% Fortunately, Gibbs sampling is an option

% Different investigation have been carry out to adapt 

% \citet{dreze1976bayesian} addressed the Bayesian problem estimation into two steps, first defining a non-informative prior to complete simultaneous equations model setting and then combining additional information to particular equation of interest. Bayesian structural equation models, that fundamentally are another way to think the instrumental variables, have been also worked by \citet{zellner1988bayesian}.

% \citet{kim2002limited}

% Alternatively, Method of Moment is a technique that pursue estimation of parameter through sample moments of distribution studied.


% However, as alternative to methods that employ full expression of likelihood, which may be difficult to derive, method of moment techniques offer another possibility, just resting on moment conditions.  In addition, these methods have proved be robust to potential misspecification REVISAR CITA. We are specially interested in situations where are being considered more moment conditions than parameters and, therefore, we are forced to manage that extra information.

% It is named as generalized when 

In the Generalized Method of Moment (GMM) framework, the main interest is in the $\beta$ parameter which can be reached minimizing the objective function

\begin{equation}
Q_n(\beta|y,\widetilde{Y},\widetilde{Z})=U_n^\top(\beta)\Sigma_n^{-1}(\beta)^{-1}U_n(\beta)
\label{quadratic}
\end{equation}

\noindent where the weighting matrix $\Sigma_n^{-1}(\beta)$ is a symmetric and positive definite matrix that could be interpreted as a distance whose particular expression is defined according to the problem is being facing. As is customary, we suppress the data dependency in last expression, and will simply write $Q_n(\beta)$ and $\widetilde{Y}=(Z,Y)$, $\beta=(\beta_1,\beta_2)$, $\widetilde{Z}=(Z,W)$ and $\Pi=(\Pi_1,\Pi_2)$. Is a well-known that IVs can be reached through GMM approach \citep{hall2005generalized} and, in this direction, \citet{zellner1996bayesian,zellner1997bayesian} constructed a Bayesian Method Of Moment (BMOM) on the foundation that there is not enough information available to form the likelihood function when both, traditional likelihood and (full) Bayesian analysis, are not feasible to perform. Curiously, no use of Bayes's Theorem was made in his posterior and predictive derivations but instead entropy associated to found moments was maximized.

% "However, if not enough information is available to specify a form for the likelihood function, then clearly there will be problems in both the traditional likelihood and Bayesian approaches"

% Of course, both frequentist and 

% On the other hand, BMOM share advantages of both, Bayesian and method of moment avoiding strong distributional assumptions about model under scrutiny and allowing sampling from posterior distribution. \citet{chamberlain2003nonparametric}, in addition, exalted the benefits related to the nonparametric application of Bayesian inference and focused their estimation procedure exploiting the structure when prior distribution belong to the Dirichlet family.

% \citet{yin2009bayesian} developed a general procedure consisting in the construction of a pseudo-posterior distribution function that depends on moment conditions instead of the likelihood expression for Generalized Linear Models (GLM). His proposal was based on that when, under some regularity conditions, $n\rightarrow\infty$, a function of a sample moment, $U_n$, generally converges to a multivariate normal distribution, and therefore $e^{-\frac{1}{2}Q_n(\beta)}\rightarrow N(0,\Sigma(\beta_0))$, where $\Sigma(\beta_0)$ is the true covariance matrix dependent on true parameter $\beta_0$. 

%\footnote{
% Formally, Proposition 1 in \citet{chernozhukov2003mcmc} \citep[see also][]{belloni2009computational}, guarantees the asymptotic normality convergence for the proper distribution

% \begin{equation}
% \pi_n(\beta|y,\widetilde{Y},\widetilde{Z})=\frac{e^{-\frac{1}{2}U_n(\beta)}\pi(\beta)}{\int_\Theta e^{-\frac{1}{2}U_n(\beta)}\pi(\beta)d\beta}\propto e^{-\frac{1}{2}U_n(\beta)}\pi(\beta) \label{qp}
% \end{equation}
%}

% Sheltered by Proposition 1 in \citet{chernozhukov2003mcmc} \citep[see also][]{belloni2009computational}, asymptotic normality convergence is guaranteed 

% \noindent the sample moment converges to a multivariate normal distribution

% full probabilistic model

% \begin{equation}
% L(y|\beta) \propto e^{-\frac{1}{2}Q_n(\beta)}
% \end{equation}

% \noindent where $\Sigma_n^{-1}(\beta)$ reduces to A QUÉ 

% Generally,

% \noindent in the GMM case, 
% \noindent It can be considered as a \emph{pseudo-posterior} where $\pi(\beta)$ was placed as prior distributions with the purpose of 
%\noindent Considering it as a \emph{pseudo-posterior} is then possible to put a prior distribution $\pi(\beta)$ over parameters and 
% achieve an approximation to true posterior distribution% through the pseudo-posterior distribution
% . In the IV case, the estimator must satisfy the moment condition $E(\widetilde{Z}^\top(y-\widetilde{Y}\beta ))$ whose sample version is given by $U_n(\beta)=\frac{1}{n}\widetilde{Z}^\top(y-\widetilde{Y}\beta_2)$ and where $\Sigma_n^{-1}(\beta)$ is approximated by $(\widetilde{Z}^\top \widetilde{Z})^{-1}$, which does not depend on either iteration $n$ or coefficient $\beta$ \citep{hayashi2000econometrics}. Note that when number of instruments is greater than parameters (overidentified case), projection matrix replaces $\widetilde{Z}^\top$ in $U_n(\beta)=\frac{1}{n}P_{\widetilde{Z}}(y-\widetilde{Z}\beta_2)$. 

% To obtain estimations of values of interest related to $\beta$, \citet{chernozhukov2003mcmc} made mention (but did not limit the use) of Markov chain Monte Carlo methods such as Metropolis–Hastings algorithm with quasi-posterior (\ref{qp}) instead of complete posterior. For cases where matrices $U_n(\beta)$ and $\Sigma_n^{-1}(\beta)$ have a more complex structure, another solutions to sample from pseudo-posterior have been proposed, mainly to accelerate the optimization procedure. Specifically, \citet{yin2012stochastic} demonstrated that their stochastic GMM estimator behaves equivalently to the classical frequentist GMM estimator.

% TAMBIÉN ACLARAR QUE CUANDO LOS INSTRUMENTOS SON MÁS QUE LAS VARIABLES ENDÓGENAS, ENTONCES SE TRABAJA CON 

Now, in order to incorporate the violation parameter into the development, we modify appropiately the setting in equations (\ref{iv_mod}) in a similar way as \citet{small2007sensitivity} did. That is, we change the response variable from $y$ to $y_\tau=y-W\tau$, yielding a new $U_{\tau,n}(\beta)=\frac{1}{n}\widetilde{Z}^\top(y_\tau-\widetilde{Y}\beta)$. Note that $\tau$ obstructs the classical estimation because if $\tau$ get a particular and known value, then we were easily able to recover the $\beta$ parameter employing traditional solutions (under $\text{cov}(W,u)=0$). Interestingly, if we treat $\tau$ as a nuisance parameter, we can easily remove its presence averaging over it. It can be carry out 

\begin{equation}
\pi(\beta|y,\widetilde{Y},\widetilde{Z})= \int e^{-\frac{1}{2}Q_{\tau,n}(\beta)} \pi(\tau,\beta)d\tau\label{proposal}
\end{equation}

% Such kind of parameter can be mostly tackled from frequentist or Bayesian perspective, 

\noindent where $\pi(\tau,\beta)$ is the joint prior distribution for $\beta$ and $\tau$ parameters. A natural choice for $\pi(\tau,\beta)$ in (IV) regression models is the normal distribution; for instance, it is possible to use two independent distributions, $\pi(\tau)\pi(\beta)$, as well as a pair of conditional distributions such as $\pi(\tau|\beta)\pi(\beta)$, responding to conceptual criteria that violation, measured by $\tau$, cannot be independent of endogenous parameter $\beta$, and it is expected to be limited to a small proportion of its value \citep{conley2012plausibly}. % REVISAR

% Even when it is a Bayesian in essence, such kind of averaging has been also worked in the frequentist statistics solving the integral $L(\psi_1)=\int L(\psi_1,\psi_2)\pi(\psi_2|\psi_1)d\psi_2$, where $L$ represents likelihood functions and $\psi=(\psi_1,\psi_2)$ are unknown parameters with $\psi_2$ being considered as `nuisance'. \citet{berger1999integrated} discussed this pointview philosophical and practically.

% under the integrated likelihood label . This marginalization process is philosophically different ours but, in practise, identical

% DECIR EN QUÉ CONSISTE LA MARGINALIZACIÓN

In the problem we are concerned, Bayesian marginalization has been applied by \citet{conley2012plausibly}, employing a Gibbs Sampling scheme. Prevented of some of the difficulties that can occur in this standard simulation-based Markov chain Monte Carlo (i.e.~poor mixing), \citet{chan2015priors} proposed an improved computational semi-analytic procedure for quantities that involve the full posterior distribution of system (\ref{iv_mod}). In particular, they utilized the posterior simulation of the identified parameters to incorporated them into a clever numerical approximation of the marginalized integral. % similar to marginalization non-identified parameters.

% \begin{equation*}
% p(\theta_i|y,s)=\int p(\theta_i|\psi)p(\psi|y,s)d\psi
% \end{equation*}

% \noindent which instead to be sampling by Bayesian sampling, it is solved and approximated analytically. For a range of different priors, those authors derived the expression HAY QUE DEJARLO UN POCO MÁS REDONDO

% \begin{equation}
% L(\beta|y)=\int L(\beta,\tau)\pi(\tau,\theta)d\tau
% \end{equation}

% \citet{Lancaster2000} commented that incidental variables have been in a `complete disarray'

% As starting point, we consider the Bayesian approach followed by \citet{conley2012plausibly}.

% Main difference with the approach employed by \citet{conley2012plausibly} is that we are not imposing a likelihood structure

% will be based on the posterior distribution for  given the data, integrating out PARAMETRO:

%. It can be viewed as an frequentist and Bayesian perspective

% Those both approaches employ explicitly a full Bayesian development in the sense that likelihood must be written down and integrated out PARA OBTENER VALORES PROMEDIOS DE LAS VARIABLES IDENTIFICABLES.

% This type of marginalization is similar to that employed when some parameters are considered as `nuisance' or `incidental'. 


% In order to compute these estimators, using Markov chain Monte Carlo methods. 

% In particular, we use the Metropolis-Hasting algorithm because ease

% \subsection{Extension to Many Instruments}

% \section{Numerical experiences}\label{ne}

\section{Simulations}

To evaluate the performance of the proposed approach in equation (\ref{proposal}), we conduct a simulation study. Specifically, we consider the system (\ref{iv_mod}) where $n=500$, $Z$ was a vector of 1's whose parameter $\beta_1=1$, acting as intercept parameter, as well as $\Pi_1$. We would like that invalid parameter were a small proportion of the value of endogenous parameter $\beta_2$, but we did not restrict the violation to that scenario holding $\beta_2=1$. For $\tau$ parameter, we choose be equal to 1 when $\ell=1$ and we select randomly values from $\{-1,0,1\}$. $\Pi_2$ was set $1/\sqrt{\ell}$ for each entry of the parameter vector. Such manner to set simulation parameters was followed by \citet{anatolyev2011specification}.

The size for instruments, $W$, was $\ell=1,3,10$. $u$ and $V$ have a joint normal distribution given by % BUSCAR DE DÓNDE EXTRAJE ESTA COVARIANZA

$$
\begin{pmatrix}
u\\
V\\
\end{pmatrix}\sim
\text{Normal} \left( 
\begin{pmatrix}
0\\
0\\
\end{pmatrix},
\begin{pmatrix}
1 & \sigma_{u,v} \\
\sigma_{u,v} & 1 \\
\end{pmatrix}
\right) 
$$


\noindent where $\sigma_{u,v}$ was varyied from three different values, $0, 0.2$ and $0.9$. $W$ also has a normal distribution with independent an identity matrix as covariance matrix. As we are also interested in the learn about the behavior of full Bayesian approximation given by \citet{conley2012plausibly}, we include in figures the estimation produced by their model. 

Prior distributions for both parameters $\beta$ and $\tau$ were set normally distributed. However, the mean and variance for $\beta$ were 0 and 100 respectively, whereas for $\tau$, mean and variance were set in such way that they coincide with mean and variance of an uniform distribution in the interval $(0,\delta)$. The rationale behind this election, as is explained by \citet{conley2012plausibly}, comes from the fact that the direct effect about violation of the exclusion restriction is assumed positive.

Results of this simulation, are summarized in Figure \ref{sim_1}.
% Results seem be more affected across the variation of $\tau$ than $\sigma_{u,v}$


\begin{figure}%[H]
\centering 
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/1-0-1.eps}
		\subcaption{} \label{sim_1:a}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/1-02-1.eps}
		\subcaption{} \label{sim_1:b}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/1-09-1.eps}
		\subcaption{} \label{sim_1:c}
	\end{minipage} 

	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/3-0-1.eps}
		\subcaption{} \label{sim_1:d}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/3-02-1.eps}
		\subcaption{} \label{sim_1:e}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/3-09-1.eps}
		\subcaption{} \label{sim_1:f}
	\end{minipage} 

	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/10-0-1.eps}
		\subcaption{} \label{sim_1:g}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/10-02-1.eps}
		\subcaption{} \label{sim_1:h}
	\end{minipage}
	\begin{minipage}[b]{0.3\linewidth}
		\centering 
		\captionsetup{justification=centering}
		\includegraphics[scale=.19, angle = -90]{images/10-09-1.eps}
		\subcaption{} \label{sim_1:i}
	\end{minipage} 

\caption{Ten simulations for 95\% Credibility Interval for endogenous parameter, $\beta_2=1$, parametrized by $\delta$. First row, panels (\subref{sim_1:a}), (\subref{sim_1:b}) and (\subref{sim_1:c}), corresponds to one instrument. Second and third rows show behavior for three and ten instruments, respectively. Levels of $\sigma_{u,v}$ are displayed across columns, i.e., first column shows $\sigma_{u,v}=0$ and last two correspond to $\in{0.2,0.9}$, subsequently. Solid lines (BLUE) correspond to the Bayesian integrated moments whereas dotted ones (RED) represent the full Bayesian approach. In addition, there were used $\Pi_2=(1, -1, -1)^\top$ for 3 instruments and $\Pi_2=(1, -1, -1,  0,  1, -1,  1,  0,  1,  1)^\top$ for 10 instruments, accordingly.
}
\label{sim_1}
\end{figure}





% It is interesting to note that in many scenarios, we were not able to recover the real endogenous parameter.

% Interval are narrower for Bayesian method of moment than for

% As number of instruments increase, both the credibility intervals turn to be more horizontal and stable, however, Bayesian method based on moment condition show less variability

% \subsection{Real data application}

% BUSCAR EJEMPLO REAL INTERESANTE

\section{Conclusions} 

\begin{center}
...{\sc To be presented in ISI2017}...
\end{center}

% It was proposed a likelihood-free method

% We believe that this divergence at the moment of judge the exclusion restriction assumption finds its origin in different literature that do have develop the IV estimation. \citet{imbens2014} made a brief digression on econometric point of view of potential outcome model and pointed out two key differences between econometric and potential outcome literature, such as that the exclusion restriction and random assignment are not clearly separated. \citet{morgan2015counterfactuals}, reflect on the fact that the causal interpretation of the regressive models (such as IV) have been lost in favor of interpretable reduction data tools.

% More complicated settings are, of course, also possible. For instance, when there are many instruments, regularization method, mainly related with lasso type estimators, have came out \citep{caner2009lasso,liao2013adaptive,hansen2014instrumental,carrasco2015regularized,windmeijer2015selecting,kang2016instrumental}. Genetic is a field that has been greatly benefited with the growing investigation in these and similar scenarios \citep{bowden2015mendelian,burgess2015review,lin2015regularization}.

% These investigations have in common that QUÉ TIENEN EN COMÚN

% Despite most of the recent research have been done under the frequentist and statistical learning data umbrella, some Bayesian articles have been published about these topics. \citet{kraay2012instrumental}, for instance, studied the systematic incorporation of prior information to covariance structure. In fields different than econometrics, Bayesian IVs has also found place, such as in Mendelian randomization \citep{mckeigue2010bayesian}.


% \section*{Acknowledgement}


% \nocite{*}

% \bibliographystyle{agsm}
% \bibliography{main}

\printbibliography

\end{document}
