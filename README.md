This repository has the poster I presented in the ISI 2017 held in Marrakech. You can find more 
information about that event in http://isi2017.org.

The title of the presentation was: «Sensitivity Analysis for Instrumental Variables Regression: A 
Nonparametric Bayesian Approach».

The abstract of my contribution is the following:

«Instrumental Variables methods play an important role in regression analysis when one or more 
predictors are endogenous. A variable qualifies as an instrument when certain assumptions are met. In 
particular, it must be uncorrelated with the error term of the structural equation, correlated with 
the endogenous variable and should not influence directly over the response variable. A Sensitivity 
Analysis approach is proposed when the exclusion restriction assumption is suspected not to hold. The 
proposed method is based upon a Nonparametric Bayesian Approach»

